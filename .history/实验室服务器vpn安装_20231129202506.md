## 实验室服务器vpn安装及使用教程

# 1. clash下载及解压(可以找适合服务器配置的clash安装文件进行下载)
- `wget https://github.com/Dreamacro/clash/releases/download/v1.15.1/clash-linux-amd64-v1.15.1.gz `
- `gzip -d clash-linux-amd64-v1.15.1.gz `

# 2. 建立文件夹，改名及给运行权限
- `mkdir ./clash `
- `mv ./clash-linux-amd64-v1.15.1 ./clash/clash `
- `cd clash `
- `chmod u+x clash `

# 3. 准备配置文件（Country.mmdb可以通过链接下载；config.yaml可以通过下面操作直接复制本地的clash的yaml文件）
（1）下载Country.mmdb
- `wget https://github.com/Dreamacro/maxmind-geoip/releases/download/20231012/Country.mmdb `

（2）复制本地的yaml文件，修改名字为config.yaml
![clash](images/screen1.png)

（3）最后,配置好的文件夹需要包含以下文件
![clash](images/screen2.png)

# 4. 打开一个终端，将clash启动起来。一定要让clash在后台一直跑着(可以打开一个screen)
- `./clash -d . `
- 正常的是这样的：
![clash](images/screen3.png)

# 5.配置终端的proxy，使其能够使用代理端口
- `export https_proxy=127.0.0.1:7890 `
- `export http_proxy=127.0.0.1:7890 `
- `export all_proxy=socks5://127.0.0.1:7890 `


# 6.检查是否连好了外网
- curl https://huggingface.co
- 连接好的结果：
![clash](images/screen8.png)
![clash](images/screen5.png)
- 不能用 ping www.google.com , 就算连了外网，这种也ping不通


# 7.如果发现连不了
- 先修改config.yaml文件，silent 改成 info，查看错误原因
![clash](images/screen6.png)
![clash](images/screen7.png)
- 解决方法：换个代理 
https://portal.dc-site5.com/#/home



